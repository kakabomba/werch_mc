from constants import *
import numpy as np
from math import log, exp, cosh, sinh, tanh, sqrt, cos, sin, asin, pi

def init_regular_L(N):  # set N ions equidistantly alternating their sorts
    state_z = (Ltube / N) * (np.arange(N) + 0.5)
    #        state_component =  np.random.randint(0,Ncomp,size=(N))
    state_component = np.empty((N,), int)
    state_component[::2] = 0
    state_component[1::2] = 1
    Nlist = [N]
    for ic in range(Ncomp): Nlist.append(np.sum(state_component == ic))
    state_L = [Nlist, state_z, state_component]
    #        N0=np.sum(state_component==0); N1=np.sum(state_component==1);
    #        state_L=[[N,N0,N1],state_z,state_component]
    return state_L


def check_state(state):
    N0 = np.sum(state[2] == 0);
    N1 = np.sum(state[2] == 1);
    Nt = len(state[1]);
    if (Nt != state[0][0] or N0 != state[0][1] or N1 != state[0][2]):
        return 1;
    else:
        return 0;


# ---INTERACTION AND ENERGIES OF CONFIGURATIONS===
# U_int = lambda z: Aa*exp(-z/rr0_A) # absolute value of pair interaction
def U_int(z1, z2):
    dz = abs(z1 - z2)
    if (dz > Ltube_2): dz = Ltube - dz
    return Aa * exp(-dz / rr0_A)


def d_energy_displace(state, n, z):  # difference of energy by moving n-th ion on a new position z
    d_en = 0.
    for item in np.delete(range(state[0][0]), n):
        d_en += q_comp[state[2][item]] * q_comp[state[2][n]] * (
                    U_int(state[1][item], z) - U_int(state[1][item], state[1][n]))
    return d_en


def d_energy_remove(state, n):  # energy gain be removing n-th ion
    d_en = 0.
    for item in np.delete(range(state[0][0]), n):
        d_en += q_comp[state[2][item]] * q_comp[state[2][n]] * U_int(state[1][item], state[1][n])
    return -d_en


def d_energy_insert(state, z, n_comp):  # energy gain by adding an ion of 'n_comp'-kind on a position z
    d_en = 0.
    for item in range(state[0][0]):
        d_en += q_comp[state[2][item]] * q_comp[n_comp] * U_int(state[1][item], z)
    return d_en


def total_energy(state):  # total energy of the system
    run = range(state[0][0]);
    en = 0.
    for item in run:
        run2 = np.delete(run, item)
        for item2 in run2:
            en += q_comp[state[2][item]] * q_comp[state[2][item2]] * U_int(state[1][item], state[1][item2])
    return en * 0.5


# ==ELEMENTARY MC PROCESSES=======================
def displace_ion(state):  # move ion along the tube
    N = state[0][0];  # Nm = state[0][1]; Np = state[0][2];
    if N == 1:
        n_ion = np.random.randint(N)
        dz = 2 * d_ion * (np.random.rand() - 0.5)
        z = state[1][0] + dz
        if z < 0.:
            state[1][0] = z + Ltube
        elif z >= Ltube:
            state[1][0] = z - Ltube
        else:
            state[1][0] = z
        if (state[1][N - 1] > Ltube):
            print("displace ALARM, N=1 state[N-1]=", state[N - 1], " n_ion=", n_ion, " dz=", dz)
            input("do something");
    elif N > 1:
        n_ion = np.random.randint(N)
        #      dz=2*d_ion*(np.random.rand()-0.5)
        dz = 4 * r_comp[state[2][n_ion]] * (np.random.rand() - 0.5)
        z = state[1][n_ion] + dz
        if n_ion == 0:
            d_hardcore0 = r_comp[state[2][0]] + r_comp[state[2][1]]
            d_hardcore1 = r_comp[state[2][N - 1]] + r_comp[state[2][0]]
            if ((z - state[1][N - 1] + Ltube > d_hardcore1) and (state[1][1] - z > d_hardcore0)):
                if z >= 0:
                    tprob = exp(-d_energy_displace(state, n_ion, z))
                    if (tprob >= 1 or tprob > np.random.rand()): state[1][n_ion] = z
                else:
                    z += Ltube
                    tprob = exp(-d_energy_displace(state, n_ion, z))
                    if (tprob >= 1 or tprob > np.random.rand()):
                        state[1] = np.append(state[1], z);
                        state[2] = np.append(state[2], state[2][0])
                        state[1] = np.delete(state[1], 0);
                        state[2] = np.delete(state[2], 0);
        elif n_ion == N - 1:
            d_hardcore0 = r_comp[state[2][N - 2]] + r_comp[state[2][N - 1]]
            d_hardcore1 = r_comp[state[2][N - 1]] + r_comp[state[2][0]]
            if ((z - state[1][N - 2] > d_hardcore0) and (state[1][0] - z + Ltube > d_hardcore1)):
                if z < Ltube:
                    tprob = exp(-d_energy_displace(state, n_ion, z))
                    if (tprob >= 1 or tprob > np.random.rand()): state[1][n_ion] = z
                else:
                    z -= Ltube
                    tprob = exp(-d_energy_displace(state, n_ion, z))
                    if (tprob >= 1 or tprob > np.random.rand()):
                        state[1] = np.insert(state[1], 0, z);
                        state[2] = np.insert(state[2], 0, state[2][n_ion])
                        state[1] = np.delete(state[1], n_ion + 1);
                        state[2] = np.delete(state[2], n_ion + 1);
        else:
            d_hardcore0 = r_comp[state[2][n_ion - 1]] + r_comp[state[2][n_ion]]
            d_hardcore1 = r_comp[state[2][n_ion]] + r_comp[state[2][n_ion + 1]]
            if ((z - state[1][n_ion - 1] > d_hardcore0) and (state[1][n_ion + 1] - z > d_hardcore1)):
                tprob = exp(-d_energy_displace(state, n_ion, z))
                if (tprob >= 1 or tprob > np.random.rand()): state[1][n_ion] = z
        if (state[1][N - 1] > Ltube):
            print("displace ALARM, state[N-1]=", state[N - 1], " n_ion=", n_ion, " dz=", dz)
            input("do something");
    return 0;


def remove_ion(state, mu_c_kBT):  # remove ion from the tube
    # READ Landau, Binder 6.1.3, p.220; Shell, Monte Carlo methods or Thermodynamics and Statistical Mechanics
    Ns = state[0];
    if Ns[0] > 0:
        n_ion = np.random.randint(Ns[0])
        #      d_en = d_energy_remove(state,n_ion)
        if Ns[0] == 1:
            d_en = 0.
        else:
            d_en = d_energy_remove(state, n_ion)
        tw = mu_c_kBT[state[2][n_ion]]
        #      tprob = exp(-(tw+d_en))*Ns[0]/Ltube;
        tprob = exp(-(tw + d_en)) * Ns[state[2][n_ion] + 1] / Ltube;
        if (tprob >= 1 or tprob > np.random.rand()):
            Ns[state[2][n_ion] + 1] -= 1
            Ns[0] -= 1;
            state[1] = np.delete(state[1], n_ion, 0)
            state[2] = np.delete(state[2], n_ion, 0)
    state[0] = Ns
    return 0;


def insert_ion(state, mu_c_kBT):  # add ion to the tube
    Ns = state[0];
    z = Ltube * np.random.rand()
    #   n_comp = np.random.randint(Ncomp)
    x_comp = np.random.rand();
    x0 = 0.;
    x1 = 0.;
    for ic in range(Ncomp):
        x1 += (Ns[ic + 1] + 1) / float(Ns[0] + Ncomp)
        if x0 <= x_comp < x1: n_comp = ic; break;
        x0 = x1
    tw = mu_c_kBT[n_comp]
    if Ns[0] == 0:
        #          tprob = exp(tw)*Ltube/(Ns[0]+1);
        tprob = exp(tw) * Ltube / (Ns[n_comp + 1] + 1);
        if (tprob >= 1 or tprob > np.random.rand()):
            state[1] = np.append(state[1], z);
            state[2] = np.append(state[2], n_comp);
            Ns[0] += 1;
            Ns[n_comp + 1] += 1;
    else:
        if z < state[1][0]:
            d_hardcore0 = r_comp[state[2][0]] + r_comp[n_comp]
            d_hardcore1 = r_comp[state[2][Ns[0] - 1]] + r_comp[n_comp]
            if ((z - state[1][Ns[0] - 1] + Ltube > d_hardcore1) and (state[1][0] - z > d_hardcore0)):
                d_en = d_energy_insert(state, z, n_comp)
                #          tprob = exp(tw-d_en)*Ltube/(Ns[0]+1);
                tprob = exp(tw - d_en) * Ltube / (Ns[n_comp + 1] + 1);
                if (tprob >= 1 or tprob > np.random.rand()):
                    state[1] = np.insert(state[1], 0, z, axis=0);
                    state[2] = np.insert(state[2], 0, n_comp, axis=0);
                    Ns[0] += 1;
                    Ns[n_comp + 1] += 1;
        elif z > state[1][Ns[0] - 1]:
            d_hardcore0 = r_comp[state[2][0]] + r_comp[n_comp]
            d_hardcore1 = r_comp[state[2][Ns[0] - 1]] + r_comp[n_comp]
            if ((z - state[1][Ns[0] - 1] > d_hardcore1) and (state[1][0] - z + Ltube > d_hardcore0)):
                d_en = d_energy_insert(state, z, n_comp)
                #          tprob = exp(tw-d_en)*Ltube/(Ns[0]+1);
                tprob = exp(tw - d_en) * Ltube / (Ns[n_comp + 1] + 1);
                if (tprob >= 1 or tprob > np.random.rand()):
                    state[1] = np.append(state[1], z);
                    state[2] = np.append(state[2], n_comp);
                    Ns[0] += 1;
                    Ns[n_comp + 1] += 1;
            if (state[1][-1] > Ltube):
                print("insert ALARM, state[-1]=", state[-1], " z>state[N-1]", " z=", z)
                input("do something");
        else:
            n_ion = 0
            #        while not(z > state[1][n_ion] and z < state[1][n_ion+1]): n_ion += 1
            while not (state[1][n_ion] < z < state[1][n_ion + 1]): n_ion += 1
            d_hardcore0 = r_comp[state[2][n_ion]] + r_comp[n_comp]
            d_hardcore1 = r_comp[state[2][n_ion + 1]] + r_comp[n_comp]
            if ((z - state[1][n_ion] > d_hardcore0) and (state[1][n_ion + 1] - z > d_hardcore1)):
                d_en = d_energy_insert(state, z, n_comp)
                #          tprob = exp(tw-d_en)*Ltube/(Ns[0]+1);
                tprob = exp(tw - d_en) * Ltube / (Ns[n_comp + 1] + 1);
                if (tprob >= 1 or tprob > np.random.rand()):
                    state[1] = np.insert(state[1], n_ion + 1, z, axis=0);
                    state[2] = np.insert(state[2], n_ion + 1, n_comp, axis=0);
                    Ns[0] += 1;
                    Ns[n_comp + 1] += 1;
    state[0] = Ns
    return 0;


# ===DEFINE MC STEPS==============================
def mc_step(state, mu_c_kBT):
    displace_ion(state);
    remove_ion(state, mu_c_kBT);  # print "remove_ion1(state)"
    displace_ion(state);
    insert_ion(state, mu_c_kBT);  # print "insert_ion1(state)"
    return 0


def thermalization(state, mu_c_kBT, Ntherm):
    i = 1
    while i <= Ntherm:
        mc_step(state, mu_c_kBT)
        i += 1
    return 0


def mc_sym(state, mu_c_kBT,
           Nsim):  # define MC symmulation, returns a list [rho*d, Q*d in AU, rho0*d, rho1*d, cap in AU]
    f1 = open('N_ion.dat', 'w')
    Nq = 0;
    Nq2 = 0;
    Nt_q = 0;
    Nsum = [0] * (Ncomp + 1)
    istep = 1
    while istep <= Nsim:
        mc_step(state, mu_c_kBT)
        for i in range(Ncomp + 1): Nsum[i] += state[0][i];  # N2sum[i] += state[0][i]**2
        Nq += (state[0][1] - state[0][2]);
        Nq2 += (state[0][1] - state[0][2]) ** 2
        Nt_q += state[0][0] * (state[0][1] - state[0][2])
        f1.write("{0}\n".format(state[0]))
        istep += 1
    Nmean = [float(x) / Nsim for x in Nsum]
    Nqmean = float(Nq) / Nsim;
    Nq2mean = float(Nq2) / Nsim;
    Nt_qmean = float(Nt_q) / Nsim;
    f2 = open('state.dat', 'w')
    np.savetxt(f2, [factor_kBT_to_eV * x for x in mu_c_kBT]);
    f2.write("\n")
    np.savetxt(f2, state[0]);
    f2.write("\n")
    np.savetxt(f2, state[1]);
    f2.write("\n")
    np.savetxt(f2, state[2])
    f2.write("\n total_energy={0}".format(total_energy(state)))
    f2.close()
    return [Nmean[0] * d_ion / Ltube, Nqmean * d_ion / Ltube, Nmean[1] * d_ion / Ltube, Nmean[2] * d_ion / Ltube,
            (Nq2mean - Nqmean ** 2) * d_ion / Ltube]
