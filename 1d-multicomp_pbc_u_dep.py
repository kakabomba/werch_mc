#import math
#from numpy import random, sort, delete
#import statistics
from constants import *
from functions import init_regular_L, thermalization, mc_sym

print('factor_kBT_to_eV=', factor_kBT_to_eV)

#mu_comp0 = map(lambda x: x/factor_kBT_to_eV, mu_comp_eV)


#---MAIN PROGRAMME===============================================================


state1=init_regular_L(N_init)
#state1=init_regular_L(N_init_sat)

f = open('1d_mc_u_dep.dat', 'w')
f.write("#r_pore={0}A, L={1}, T={2}K, Nsimul={3}, Nsimul={4}\n".format(r_pore_A,Ltube,temp,Nsimul,Ntherm))
f.write("#u,V \t d*rho \t charge, mu*C/cm2 \t d_ion*rho_+ \t d_ion*rho_- \t capacitance, muF/cm2 \t capacitance, in AU \n")
f.close()

w_eV=0.
w_kBT=w_eV/factor_kBT_to_eV
u0_eV = 0.; u1_eV = 0.5; nu = 5; du = (u1_eV-u0_eV)/nu;
u_eV_list=[u0_eV+i*du for i in range(0, nu+1)]
u_list=[x/factor_kBT_to_eV for x in u_eV_list]


for v_u in u_list:
#  mu_comp=[w_kBT+q_comp[0]*v_u,w_kBT+q_comp[1]*v_u]
  mu_comp=[w_kBT+x*v_u for x in q_comp ]
#  state1=init_regular_L(N_init_sat)
  thermalization(state1,mu_comp,Ntherm)
  data_sym=mc_sym(state1,mu_comp,Nsimul)
#  Ndata = mc_sym0(state1,mu_comp,Nsimul)
#  data_sym = mc_eval(Ndata)
  f = open('1d_mc_u_dep.dat', 'a')
#  f.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n".format(v_u*factor_kBT_to_eV,data_sym[0],q_coeff*100 *data_sym[1],data_sym[2],data_sym[3],C_coeff*100 *data_sym[4],data_sym[5]))
  f.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\n".format(v_u*factor_kBT_to_eV,data_sym[0],q_coeff*100 *data_sym[1],data_sym[2],data_sym[3],C_coeff*100 *data_sym[4],data_sym[4]))
  f.close()
  print('u=',v_u*factor_kBT_to_eV, 'density=', data_sym[0],q_coeff*100 *data_sym[1], data_sym[2],data_sym[3],C_coeff*100 *data_sym[4],data_sym[4])


