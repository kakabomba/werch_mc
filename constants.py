from math import pi

#---INPUT DATA=====================================
Ntherm = 300000 # number of the thermalization steps
Nsimul = 1000000 # number of the simulation steps
Ltube = 250. # Angstrom
Ncomp=2 # number of components: 0/1 -- "+/-" ion
q_comp=[1,-1] # charge in charge of an electron
r_comp = [2.5,2.5] # ion radii in Angstrom

temp=300 #K
r_pore_A=2.6 # Angstrom
#------------------------------------------------

rr0_A=r_pore_A/2.4  # interaction radius in Angstrom U(x)=A*exp(-x/rr0_A)
d_ion = 2*max(r_comp)  # Angstrom
N_init = int(Ltube/(2*d_ion)) # half packing
N_init_sat = int(Ltube/d_ion) - 1 # almost saturated packing
Ltube_2 = Ltube/2.

#--PHYSICAL CONSTANTS============================
k_B = 1.38064852e-23 # J/K
e_charge = 1.6e-19 # C
k_e = 8.9875517923e9 # N*m^2/C^2,  Coulomb constant, the electric force constant, or the electrostatic constant
eps_r = 2.5 # dielectric constant inside pore
lB = k_e*e_charge*e_charge/(eps_r*k_B*temp) # Bjerrum length
lB_A = lB*1.e10
#lB = 22. #22.24
Aa = 3.08*lB_A/r_pore_A # interaction constant U(x)=A*exp(-x/rr0_A)

factor_J_to_eV=6.242e18
factor_K_to_eV=8.61732814974056E-05
factor_kBT_to_eV=temp*k_B*factor_J_to_eV
C_coeff = e_charge*e_charge / (2.*pi*r_pore_A * 1.e-10 * d_ion * 1.e-10 * k_B * temp)
q_coeff = e_charge / (2.*pi*r_pore_A * 1.e-10 * d_ion * 1.e-10)